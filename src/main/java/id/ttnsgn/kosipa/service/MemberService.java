package id.ttnsgn.kosipa.service;

import java.util.List;
import id.ttnsgn.kosipa.model.entity.core.MemberEntity;

public interface MemberService {
    List<MemberEntity> getAllMember();
}
