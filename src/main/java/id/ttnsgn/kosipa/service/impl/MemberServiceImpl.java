package id.ttnsgn.kosipa.service.impl;

import id.ttnsgn.kosipa.model.entity.core.MemberEntity;
import id.ttnsgn.kosipa.repository.MemberRepository;
import id.ttnsgn.kosipa.service.MemberService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberRepository memberRepository;

    @Override
    public List<MemberEntity> getAllMember() {
        return memberRepository.findAll();
    }
}
