package id.ttnsgn.kosipa.repository;

import id.ttnsgn.kosipa.model.entity.core.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AccountRepository extends JpaRepository<AccountEntity, UUID> {
}