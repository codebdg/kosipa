package id.ttnsgn.kosipa.repository;

import id.ttnsgn.kosipa.model.entity.core.MemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MemberRepository extends JpaRepository<MemberEntity, UUID> {
}