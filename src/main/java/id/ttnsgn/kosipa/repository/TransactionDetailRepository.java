package id.ttnsgn.kosipa.repository;

import id.ttnsgn.kosipa.model.entity.core.TransactionDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface TransactionDetailRepository extends JpaRepository<TransactionDetailEntity, UUID> {
}