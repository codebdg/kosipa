package id.ttnsgn.kosipa.repository;

import id.ttnsgn.kosipa.model.entity.core.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface TransactionRepository extends JpaRepository<TransactionEntity, UUID> {
}