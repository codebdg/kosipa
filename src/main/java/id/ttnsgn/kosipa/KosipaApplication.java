package id.ttnsgn.kosipa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KosipaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KosipaApplication.class, args);
	}

}
