package id.ttnsgn.kosipa.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import id.ttnsgn.kosipa.model.dto.*;
import java.util.List;

@RestController
public class MemberController {

    @GetMapping(value = "/members")
    public ResponseEntity<List<Member>> getAllMember() {
        return ResponseEntity.ok(null);
    }
}
