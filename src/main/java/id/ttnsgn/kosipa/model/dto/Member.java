package id.ttnsgn.kosipa.model.dto;

import java.sql.Timestamp;
import java.util.UUID;

public class Member {
    private UUID id;
    private String name;
    private Timestamp dob;
    private String address;
}
