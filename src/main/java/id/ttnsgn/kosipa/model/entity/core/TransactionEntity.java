package id.ttnsgn.kosipa.model.entity.core;

import id.ttnsgn.kosipa.common.TransactionType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "transaction", schema = "core", catalog = "kosipa")
public class TransactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pg-uuid")
    @GenericGenerator(name = "pg-uuid", strategy = "uuid2", parameters = @Parameter(name = "uuid_gen_strategy_class", value = "id.ttnsgn.kosipa.model.entity.PostgreSQLUUIDGenerationStrategy"))
    @Column(name = "id", nullable = false)
    private UUID id;

    @CreationTimestamp
    @Column(name = "timestamp")
    private Timestamp timestamp;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private AccountEntity account;

    @Column(name = "balance")
    private Double balance;

    @Column(name = "status")
    private Integer status;

    @OneToOne(mappedBy = "transaction")
    private TransactionDetailEntity transactionDetail;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = true, length = 10)
    private TransactionType type;

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public TransactionDetailEntity getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(TransactionDetailEntity transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public AccountEntity getAccount() {
        return account;
    }

    public void setAccount(AccountEntity account) {
        this.account = account;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
