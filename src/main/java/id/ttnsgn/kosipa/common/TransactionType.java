package id.ttnsgn.kosipa.common;

public enum TransactionType {
    DEPOSIT, WITHDRAW
}